

var container=document.getElementById('visualizar');
var horario=document.getElementById('visualizar_horario');
var registrar= document.getElementById('registrar_horario');
var contenedor_horario=document.getElementById('contenedor_horario');
var labs=document.getElementById('labs');
var aulas=document.getElementById('aulas');
var sistemas=document.getElementById('sistemas');
var sistemashorario=document.getElementById('sistemas_hora');
var tihorario=document.getElementById('ti_hora');
var laboratorio =document.getElementById('laboratorio');
var container_sistemas=document.getElementById('opciones_sistemas');
var container_ti=document.getElementById('opciones_ti');
var pie_pagina=document.getElementById('pie_pagina');
var gestion_horario=document.getElementById('gestion_horario');
var descripcion=document.getElementById('descripcion');
var eliminar_h=document.getElementById('eliminar');
var tabla1=document.getElementById('tabla1');
var editar=document.getElementById('editar');

editar.addEventListener('click',editar_horario)
eliminar_h.addEventListener("click", eliminarHorario);
sistemas.addEventListener('mouseover', opciones_horario);
sistemas.addEventListener('mouseout', opciones_horario2);
sistemashorario.addEventListener("click", horariosSistemas);
tihorario.addEventListener("click", horariosti);
laboratorio.addEventListener("click",mostrar_laboratorios)
horario.addEventListener("click", cambiar);
registrar.addEventListener("click", registro_horario);
//sistemashorario.addEventListener("click",mostrar_sistemas);
tihorario.addEventListener("click", mostrar_ti);





function opciones_horario(){
    descripcion.style.display="block"
    gestion_horario.style.display="flex";
    sistemas.style.transition="1.0s";
    gestion_horario.style.transition="1.0s";
    sistemas.style.opacity="1.0";
    sistemas.style.backgroundColor="rgb(141, 137, 137)";
}
function opciones_horario2(){
    descripcion.style.display="none"
    sistemas.style.display="block";
    gestion_horario.style.display="none";
    sistemas.style.transition="1.0s";
    gestion_horario.style.transition="0.5s";
    sistemas.style.backgroundColor="white";
}
function eliminarHorario(){
    tabla1.style.display="none";
    alert("Se eliminó de manera exitosa");
}
function editar_horario(){
    location="./editar_horario.html"
}




function mostrar_ti(){
    tihorario.style.display="block"
}
function mostrar_sistemas(){
    container_sistemas.style.display="block";
}
function mostrar_laboratorios(){

    contenedor_horario.style.display="block";
    container.style.display="none"
    aulas.style.display="none"
    labs.style.display="block";
    pie_pagina.style.marginBlockStart="300px";
}
function cambiar(){
    
    container.style.display="none"
    contenedor_horario.style.display="block"
    pie_pagina.style.marginBlockStart="1000px";
   
}
function registro_horario(){
    location="./registroHorario.html"
}

function horariosSistemas(){
    container.style.display="none"
    contenedor_horario.style.display="block"
    aulas.style.display="block"
    labs.style.display="none";
    pie_pagina.style.marginBlockStart="400px";
}
function horariosti(){
    container.style.display="none"
    contenedor_horario.style.display="block"
    aulas.style.display="block"
    labs.style.display="none";
    pie_pagina.style.marginBlockStart="400px";
}

function doSearch()
{
    const tableReg = document.getElementById('datos');
    const searchText = document.getElementById('searchTerm').value.toLowerCase();
    let total = 0;

    // Recorremos todas las filas con contenido de la tabla
    for (let i = 1; i < tableReg.rows.length; i++) {
        // Si el td tiene la clase "noSearch" no se busca en su cntenido
        if (tableReg.rows[i].classList.contains("noSearch")) {
            continue;
        }

        let found = false;
        const cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
        // Recorremos todas las celdas
        for (let j = 0; j < cellsOfRow.length && !found; j++) {
            const compareWith = cellsOfRow[j].innerHTML.toLowerCase();
            // Buscamos el texto en el contenido de la celda
            if (searchText.length == 0 || compareWith.indexOf(searchText) > -1) {
                found = true;
                total++;
            }
        }
        if (found) {
            tableReg.rows[i].style.display = '';
        } else {
            // si no ha encontrado ninguna coincidencia, esconde la
            // fila de la tabla
            tableReg.rows[i].style.display = 'none';
        }
    }

    // mostramos las coincidencias
    const lastTR=tableReg.rows[tableReg.rows.length-1];
    const td=lastTR.querySelector("td");
    lastTR.classList.remove("hide", "red");
    if (searchText == "") {
        lastTR.classList.add("hide");
    } else if (total) {
        td.innerHTML="Se ha encontrado "+total+" coincidencia"+((total>1)?"s":"");
    } else {
        lastTR.classList.add("red");
        td.innerHTML="No se han encontrado coincidencias";
    }
}